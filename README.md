Dropull - Alternative Dropbox Script
====================================

## Install

    $ mkdir -p ~/local
    $ cd ~/local
    $ git clone git://github.com/nyarla/p5-Dropull.git Dropull
    $ cd Dropull
    $ cpanm -v --installdeps .

## How to use

    $ mkdir ~/Dropull
    $ export DROPULL_REMOTE=git@github.com:{yourID}/{yourRepo}.git
    $ ~/local/Dropull/dropull.pl

## More information

Dropull is writted by Perl.

And Dropull source code is very simple.

If you want to more information of Dropull,
See source code ;-)

## Original Idea

Dropull is inspired meltingice's RubyDrop

* [meltingice/RubyDrop](https://github.com/meltingice/RubyDrop)

## Author and Contact

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net* (Japanese)

Twitter: [@nyarla](https://twitter.com/nyarla)
Facebook: [@nyarlax](https://www.facebook.com/nyarlax)

## License

Dropull is under public domain.

So, Fork and Hack ME!
