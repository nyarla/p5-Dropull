#!/usr/bin/env perl

use strict;
use warnings;

use Log::Minimal;
use File::chdir;
use AnyEvent;

my $rootdir  = $ENV{'DROPULL_ROOT'}   || $ENV{'HOME'} . '/Dropull';
my $remote   = $ENV{'DROPULL_REMOTE'}
    or die 'env value of `DROPULL_REMOTE` is not found.';
my $interval = $ENV{'DROPULL_INTERVAL'} || 5; 

# setup Dropull repository
if ( ! -d "${rootdir}/.git" ) {
    infof("Setup git repository: %s", $rootdir);

    local $CWD = $rootdir;

    system(qw/git init/);
    system(qw/git remote add origin/, $remote);
}

sub parse_revs {
    my $line = shift;
    my ( $data ) = ( ($line || q{} ) =~ m{^([a-f0-9]{40})\s} );

    if ( ! defined $data || $data eq q{} ) {
        $data = '(empty)';
    }

    return $data;
}

# start check git repository
my $cv = AnyEvent->condvar;
my $watcher = AnyEvent->timer(
    after    => $interval,
    interval => $interval,
    cb       => sub {
        local $CWD = $rootdir;

        my $remote_rev = parse_revs(`git ls-remote origin HEAD`);
        my $local_rev  = parse_revs(`git rev-parse HEAD`); 

        infof("Current remote: %s", $remote_rev);
        infof("Current local: %s", $local_rev);
       
        if ( $remote_rev ne '(empty)' && $remote_rev ne $local_rev ) {
            infof('Sync remote -> local');
            system(qw/git reset --hard HEAD/);
            system(qw/git pull origin master/);
            infof('Finished.');
        }

        if ( !! `git status --porcelain` ) {
            infof('Sync local -> remote');
            system(qw/git add ./);
            system(qw/git commit -m "updated."/);
            system(qw/git push origin master/);
        }
    },
);
my $sig; $sig = AnyEvent->signal(
    signal => 'QUIT',
    cb     => sub {
        undef $watcher;
        undef $sig;
    },
);

$cv->recv;
